import { Params, ActivatedRoute, Router } from '@angular/router';
import { RecipeService } from './../recipe.service';
import { Recipe } from './../recipe.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {
  recipe: Recipe;
  id: number;

  constructor(private recipeService: RecipeService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {    
    this.route.params.subscribe(
      (params: Params) => { 
        this.id = +params['id'];   
        this.recipe = this.recipeService.getRecipe(this.id);
     });
  }

  onGoToShoppingList() {
    this.recipeService.addToShoppingList(this.recipe);
    this.router.navigate(['/shopping-list']);
  }

  onRemoveRecipe() {
    this.recipeService.removeRecipe(this.id);
    this.router.navigate(['/recipes']);
  }
}
