import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-empty-recipe-item',
  templateUrl: './empty-recipe-item.component.html',
  styleUrls: ['./empty-recipe-item.component.css']
})
export class EmptyRecipeItemComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
